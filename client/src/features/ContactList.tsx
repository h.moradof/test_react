import React, { useState } from 'react';
import { IContactItem } from '../api/data';

interface IProps {
    contacts: IContactItem[],
    displayEditForm: (id: number) => void,
    deleteContact: (id: number) => void
}

const ContactList: React.FC<IProps> = ({ contacts, displayEditForm, deleteContact }) => {


    const [searrchText, setSearchText] = useState<string>('');

    const search = (event: any) => {
        const txt = event.target.value;
        setSearchText(txt);
    }



    return (
        <section className="card">
            <input type="text" onChange={search} className="search" placeholder='search by name' />
            <br />
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>mobile</th>
                        <th>tel</th>
                        <th>email</th>
                        <th className='actions'></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        contacts.filter((c) => searrchText === '' || c.name.startsWith(searrchText))
                        .map((contact) => (
                            <tr key={contact.id}>
                                <td>{contact.id}</td>
                                <td>{contact.name}</td>
                                <td>{contact.mobile}</td>
                                <td>{contact.tel}</td>
                                <td>{contact.email}</td>
                                <td>
                                    <button onClick={() => displayEditForm(contact.id)} className="blue-light" type="button">Edit</button>
                                    <button onClick={() => deleteContact(contact.id)} className="red" type="button">Delete</button>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>
        </section>
    );
}

export default ContactList
