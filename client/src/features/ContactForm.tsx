import React, { useState } from 'react'
import { IContactItem } from '../api/data';

interface IProps {
    selectedContact: IContactItem | null,
    setEditMode: (editMode: boolean) => void,
    submitForm: (contact: IContactItem) => void
}

const ContactForm: React.FC<IProps> = ({ selectedContact, setEditMode, submitForm }) => {

    const initializeForm = () => {
        if (selectedContact) {
            return selectedContact;
        }
        else {
            return {
                id: 0,
                name: '',
                mobile: '',
                tel: '',
                email: ''
            }
        }
    };

    // state for form model  
    const [model, setModel] = useState<IContactItem>(initializeForm);


    const onChangeHandler = (event: any) => {
        const { name, value } = event.target;
        setModel({ ...model, [name]: value });
    }


    return (
        <div className="card">
            <div className="form-group">
                <input
                    type="text"
                    onChange={onChangeHandler}
                    name='name'
                    placeholder='name'
                    value={model.name} />
            </div>
            <div className="form-group">
                <input
                    type="text"
                    onChange={onChangeHandler}
                    name='mobile'
                    placeholder='mobile'
                    value={model.mobile} />
            </div>
            <div className="form-group">
                <input
                    type="text"
                    onChange={onChangeHandler}
                    name='tel'
                    placeholder='tel'
                    value={model.tel} />
            </div>
            <div className="form-group">
                <input
                    type="email"
                    onChange={onChangeHandler}
                    name='email'
                    placeholder='email'
                    value={model.email} />
            </div>
            <div className="form-group">
                <button className="blue" onClick={() => submitForm(model)}>save</button>
                <button className="white" onClick={() => setEditMode(false)}>cancel</button>
            </div>
        </div>
    )
}

export default ContactForm