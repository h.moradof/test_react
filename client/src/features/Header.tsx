import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <header>
                <h1>Welcome to ReactJSX Contact Book</h1>
            </header>
        )
    }
}


export default Header;