import React, { Fragment, useState, useEffect } from 'react';
import Header from '../../features/Header';
import ContactForm from '../../features/ContactForm';
import ContactList from '../../features/ContactList';
import { contactsData, IContactItem } from '../../api/data';

const App = () => {

  const [contacts, setContacts] = useState<IContactItem[]>([]); // a state to keep contact list 
  const [editMode, setEditMode] = useState(false); // a state to make a decision of Show/Hide Form
  const [selectedContact, setSelectedContact] = useState<IContactItem | null>(null); // a state to keep updating contact

  const handleDisplayCreateContactForm = () => {
    setEditMode(true); // show form
    setSelectedContact(null); // in create mode
  }

  const handleDiaplayEditContactForm = (id: number) => {
    setEditMode(true); // show form
    setSelectedContact(contacts.filter(c => c.id === id)[0]); // in edit mode
  }

  const handleSubmitContactForm = (model: IContactItem) => {
    if (model.id == 0) {
      // insert new contact
      //set id 
      let maxId = 0;
      if(contacts.length > 0)
      {
        maxId = Math.max.apply(Math, contacts.map(function (item) { return item.id; }));
      }
      model.id = maxId + 1;
      contacts.push(model);
    }
    else {
      // update existing contact
      var updatingItem = contacts.filter(c => c.id === model.id)[0];
      updatingItem.name = model.name;
      updatingItem.mobile = model.mobile;
      updatingItem.tel = model.tel;
      updatingItem.email = model.email;
    }

    // reset all
    setEditMode(false);
    setSelectedContact(null);
  }

  const handleDeleteContact = (id: number) => {
    setContacts(contacts.filter(c => c.id !== id));

    // reset all
    setEditMode(false);
    setSelectedContact(null);
  }


  useEffect(() => {
    setContacts(contactsData); // just for now
  }, []);
  // second parameter prevent executing the code every time state props sets or changes


  return (
    <Fragment>
      <Header />
      <section className="col-left">

        <button
          onClick={handleDisplayCreateContactForm}
          className="green new">Create new Contact</button>

        <ContactList
          contacts={contacts}
          displayEditForm={handleDiaplayEditContactForm}
          deleteContact={handleDeleteContact} />

      </section>
      <section className="col-right">
        {
          editMode &&
          <ContactForm
            selectedContact={selectedContact}
            setEditMode={setEditMode}
            submitForm={handleSubmitContactForm} />
        }
      </section>
      <div className="clearing"></div>
    </Fragment>
  );
}

export default App;
