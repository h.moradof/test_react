export interface IContactItem {
    name: string,
    tel?: string,
    mobile : string,
    email? : string,
    id : number
}

const item1 : IContactItem = {
    id: 1,
    name: 'ali',
    mobile : '09128887766',
    tel: '021888724434'
}

const item2 : IContactItem = {
    id : 2,
    name: 'hasan',
    mobile : '09350009988',
    email : 'test@gmail.com'
}

const item3 : IContactItem = {
    id:3,
    name: 'rerza',
    mobile : '09123334455'
}

export const contactsData = [item1, item2, item3];