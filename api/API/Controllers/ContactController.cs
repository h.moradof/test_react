﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Contacts.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {

        #region ctor
        private readonly IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        #endregion


        [HttpPost]
        public async Task Post(Contact model)
        {
            await _contactService.Add(model);
        }

        [HttpPut]
        public async Task Put(Contact model)
        {
            await _contactService.Edit(model);
        }


        [HttpGet]
        public async Task<IEnumerable<Contact>> Get()
        {
            return await  _contactService.GetAll();
        }



        [HttpDelete]
        [Route("{id}")]
        public async Task Delete(int id)
        {
            await _contactService.Remove(id);
        }
    }
}
