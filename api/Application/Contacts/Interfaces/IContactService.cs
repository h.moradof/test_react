﻿using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Contacts.Interfaces
{
    public interface IContactService
    {
        Task<IEnumerable<Contact>> GetAll();
        Task Add(Contact model);
        Task Edit(Contact model);
        Task Remove(int id);
    }
}
