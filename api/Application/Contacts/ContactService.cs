﻿using Application.Contacts.Interfaces;
using Domain.Contracts;
using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Contacts
{
    public class ContactService : IContactService
    {

        #region ctor
        private readonly IContactRepository _contactRepository;

        public ContactService(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }
        #endregion


        public async Task Add(Contact model)
        {
            await _contactRepository.Add(model);
        }

        public async Task Remove(int id)
        {
            await _contactRepository.Remove(id);
        }

        public async Task Edit(Contact model)
        {
            await _contactRepository.Edit(model);
        }

        public async Task<IEnumerable<Contact>> GetAll()
        {
            return await _contactRepository.GetAll();
        }

    }
}
