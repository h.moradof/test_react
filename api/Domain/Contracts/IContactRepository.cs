﻿using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Contracts
{
    public interface IContactRepository
    {
        Task Add(Contact model);
        Task Edit(Contact model);
        Task<IEnumerable<Contact>> GetAll();
        Task Remove(int id);
    }
}
