﻿using Domain.Contracts;
using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ContactRepository : IContactRepository
    {

        // test
        private static List<Contact> _db = new List<Contact> 
        {
            new Contact{ Id = 1, Name = "ahmad", Mobile = "09123234234", Email = "", Tel = "" },
            new Contact{ Id = 2, Name = "hasan", Mobile = "09345465466", Email = "", Tel = "0261345837458" },
            new Contact{ Id = 3, Name = "saman", Mobile = "093362347348", Email = "test@test.test", Tel = "021234345345" }
        };


        public async Task Add(Contact model)
        {
            _db.Add(model);
        }

        public async Task Edit(Contact model)
        {
            var item = _db.FirstOrDefault(c => c.Id == model.Id);
            if (item != null)
            {
                item.Name = model.Name;
                item.Mobile = model.Mobile;
                item.Tel = model.Tel;
                item.Email = model.Email;
            }
        }

        public async Task<IEnumerable<Contact>> GetAll()
        {
            return _db;
        }

        public async Task Remove(int id)
        {
            var item = _db.FirstOrDefault(c => c.Id == id);
            if(item!= null)
            {
                _db.Remove(item);
            }
        }
    }
}
